//  
//  PagginationViewController.swift
//  Paggination
//
//  Created by Anton Vishnevsky on 30/10/2019.
//  Copyright © 2019 Wishell. All rights reserved.
//

import UIKit

final class PagginationViewController: UIViewController {
    
    var dataSource: PagginationDataSource!
    var model: PagginationModelInput!
    lazy var contentView: PagginationViewInput = { return view as! PagginationViewInput }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataSource = PagginationDataSource()
        contentView.dataSource = dataSource
        model.load()
    }
    
}

// MARK: - PagginationModelOutput
extension PagginationViewController: PagginationModelOutput {
    
    func modelDidFail(with: Error) {
        
    }
    
    func modelDidLoad(with films: Films) {
        dataSource.listOfFilms = films
    }
}

// MARK: - PagginationViewControllerInput
extension PagginationViewController: PagginationViewControllerInput {}
