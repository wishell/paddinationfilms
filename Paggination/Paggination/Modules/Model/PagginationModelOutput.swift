//  
//  PagginationModelOutput.swift
//  Paggination
//
//  Created by Anton Vishnevsky on 30/10/2019.
//  Copyright © 2019 Wishell. All rights reserved.
//

protocol PagginationModelOutput: class {
    func modelDidLoad(with films: Films)
    func modelDidFail(with: Error)
}
