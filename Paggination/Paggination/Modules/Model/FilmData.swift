//
//  FilmData.swift
//  Paggination
//
//  Created by Anton Vishnevsky on 30/10/2019.
//  Copyright © 2019 Wishell. All rights reserved.
//

import Foundation

// MARK: - FilmElement
struct FilmElement: Codable {
    let countries: String
    let duration: Int
    let partner: Partner
    let actors: String?
    let image: String
    let contentGroupPtr, favorites, adult: Int
    let activityDescription, genres: String
    let year, contentTypePtr: Int
    let partnerPath, added: String
    let id, locked: Int
    let name: String
    let comments: Comments
    let director: String?
    let created: String
    let age: Age
    let status: Int

    enum CodingKeys: String, CodingKey {
        case countries, duration, partner, actors, image
        case contentGroupPtr = "content_group_ptr"
        case favorites, adult
        case activityDescription = "description"
        case genres, year
        case contentTypePtr = "content_type_ptr"
        case partnerPath = "partner_path"
        case added, id, locked, name, comments, director, created, age, status
    }
}

enum Age: String, Codable {
    case the12 = "12+"
    case the16 = "16+"
    case the18 = "18+"
    case the6 = "6+"
}

enum Comments: String, Codable {
    case epg = "epg"
    case startMovieAbsolyutnayaVlast = "start__/movie/absolyutnaya-vlast"
    case startMovieBolshayaIgra = "start__/movie/bolshaya-igra"
    case startMovieEkstrim = "start__/movie/ekstrim"
    case startMovieMestoVstrechi = "start__/movie/mesto-vstrechi"
    case startMoviePersonalnyyPokupatel = "start__/movie/personalnyy-pokupatel"
    case startMovieUbiystvoSvyashchennogoOlenya = "start__/movie/ubiystvo-svyashchennogo-olenya"
    case tvzavr220 = "tvzavr_220"
}

enum Partner: String, Codable {
    case empty = ""
    case start = "start"
    case tvzavr = "tvzavr"
}

typealias Films = [FilmElement]
