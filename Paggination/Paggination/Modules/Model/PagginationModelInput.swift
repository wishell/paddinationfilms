//  
//  PagginationModelInput.swift
//  Paggination
//
//  Created by Anton Vishnevsky on 30/10/2019.
//  Copyright © 2019 Wishell. All rights reserved.
//

protocol PagginationModelInput {
    func load()
}
