//  
//  PagginationModel.swift
//  Paggination
//
//  Created by Anton Vishnevsky on 30/10/2019.
//  Copyright © 2019 Wishell. All rights reserved.
//

import Foundation

final class PagginationModel: PagginationModelInput {
    
    weak var output: PagginationModelOutput!
    
    func load() {
        let file = Bundle.main.path(forResource: "FilmsData", ofType: "json")!
        let url  = URL(fileURLWithPath: file)
        if let data = try? Data(contentsOf: url) {
            do {
                let films = try JSONDecoder().decode(Films.self, from: data)
                output.modelDidLoad(with: films)
            } catch {
                //modelDidFail{}
            }
        } else {
//            modelDidFail(error!)
        }
        
    }
    
}
