//  
//  PagginationModuleConfigurator.swift
//  Paggination
//
//  Created by Anton Vishnevsky on 30/10/2019.
//  Copyright © 2019 Wishell. All rights reserved.
//

import UIKit

final class PagginationModuleConfigurator {
    
    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {
        if let viewController = viewInput as? PagginationViewController {
            configure(viewController: viewController)
        }
    }
    
    private func configure(viewController: PagginationViewController) {
        let model = PagginationModel()
        model.output = viewController
        
        viewController.model = model
    }
    
}
