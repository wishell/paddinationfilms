//  
//  PagginationModuleInitializer.swift
//  Paggination
//
//  Created by Anton Vishnevsky on 30/10/2019.
//  Copyright © 2019 Wishell. All rights reserved.
//

import UIKit

final class PagginationModuleInitializer: NSObject {
    
    @IBOutlet weak var viewController: PagginationViewController!
    
    override func awakeFromNib() {
        let configurator = PagginationModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: viewController)
    }
    
}
