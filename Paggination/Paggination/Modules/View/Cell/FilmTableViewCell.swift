//
//  FilmTableViewCell.swift
//  Paggination
//
//  Created by Anton Vishnevsky on 30/10/2019.
//  Copyright © 2019 Wishell. All rights reserved.
//

import UIKit

class FilmTableViewCell: UITableViewCell, NibLoadableType, ReusableViewType {

    @IBOutlet weak var filmImg: LoadableImage!
    @IBOutlet weak var titleL: UILabel!
    @IBOutlet weak var genre: UILabel!
    @IBOutlet weak var countryL: UILabel!
    
    func configure(for film: FilmElement) {
        filmImg.downloadImageFrom(urlString: film.image)
        titleL.text = film.name
        genre.text = film.genres
        countryL.text = film.countries
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        filmImg.cancelTask()
    }
    
}
