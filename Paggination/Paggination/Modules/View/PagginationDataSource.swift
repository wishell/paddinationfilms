//
//  PagginationDataSource.swift
//  Paggination
//
//  Created by Anton Vishnevsky on 30/10/2019.
//  Copyright © 2019 Wishell. All rights reserved.
//

import UIKit

final class PagginationDataSource: NSObject {
    
    private var index = 0
    private var limit = 10
    private var recordsArray: Films = []
    private weak var tableV: UITableView!
    var listOfFilms: Films = [] {
        didSet { addRecords() }
    }
    
    func attach(to tableV: UITableView) {
        self.tableV = tableV
        tableV.delegate = self
        tableV.dataSource = self
        tableV.estimatedRowHeight = 120
        tableV.register(FilmTableViewCell.self)
    }
    
    private func addRecords() {
        while index < limit {
            if index < listOfFilms.count{
                recordsArray.append(listOfFilms[index])
                index += 1
            } else { return }
        }
    }
    
    @objc
    private func loadTable() {
        tableV.reloadData()
    }
    
}

extension PagginationDataSource: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        recordsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: FilmTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        cell.configure(for: recordsArray[indexPath.row])
        return cell
    }
    
    
}

extension PagginationDataSource: UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == recordsArray.count - 1 {
            index = recordsArray.count - 1
            limit = recordsArray.count + 10
            addRecords()
            perform(#selector(loadTable), with: nil, afterDelay: 0.3)
        }
    }
    
}
