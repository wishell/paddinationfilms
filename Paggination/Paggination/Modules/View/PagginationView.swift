//  
//  PagginationView.swift
//  Paggination
//
//  Created by Anton Vishnevsky on 30/10/2019.
//  Copyright © 2019 Wishell. All rights reserved.
//

import UIKit

protocol PagginationViewInput: class {
    var dataSource: PagginationDataSource! { get set }
}

final class PagginationView: UIView {
    
    @IBOutlet weak var tableV: UITableView!
    
    var dataSource: PagginationDataSource! {
        get { return tableV.dataSource as? PagginationDataSource }
        set { newValue?.attach(to: tableV) }
    }
    
}

// MARK: - PagginationViewInput
extension PagginationView: PagginationViewInput {}
