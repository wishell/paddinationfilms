//
//  NibLoadableType.swift
//  Paggination
//
//  Created by Anton Vishnevsky on 30/10/2019.
//  Copyright © 2019 Wishell. All rights reserved.
//

import UIKit

protocol NibLoadableType: class {
    static var nibName: String { get }
    static var nib:     UINib { get }
}

extension NibLoadableType {
    
    static var nibName: String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
    
    static var nib: UINib {
        let bundle = Bundle(for: Self.self)
        let nib    = UINib(nibName: Self.nibName, bundle: bundle)
        return nib
    }
    
    
}

extension NibLoadableType where Self: UIView {

    static func fromNib() -> Self {
        return nib.instantiate(withOwner: nil, options: nil).first as! Self
    }

}
