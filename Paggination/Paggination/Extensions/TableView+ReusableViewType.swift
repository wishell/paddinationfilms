//
//  TableView+ReusableViewType.swift
//  Paggination
//
//  Created by Anton Vishnevsky on 30/10/2019.
//  Copyright © 2019 Wishell. All rights reserved.
//

import UIKit

protocol ReusableViewType: class {
    static var defaultReuseIdentifier: String { get }
}

extension ReusableViewType where Self: UIView {

    static var defaultReuseIdentifier: String {
        return NSStringFromClass(self)
    }

}
