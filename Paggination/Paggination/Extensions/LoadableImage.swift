//
//  LoadableImage.swift
//  Paggination
//
//  Created by Anton Vishnevsky on 30/10/2019.
//  Copyright © 2019 Wishell. All rights reserved.
//

import UIKit

class LoadableImage: UIImageView {
    
    private let imageCache = NSCache<NSString, AnyObject>()
    private var currentTask: URLSessionDataTask!
    
    func cancelTask(){
        currentTask.cancel()
    }
    
    func downloadImageFrom(urlString: String) {
        guard let url = URL(string: urlString) else { return }
        if let cachedImage = imageCache.object(forKey: url.absoluteString as NSString) as? UIImage {
            self.image = cachedImage
        } else {
            downloadImageFrom(url: url)
        }
    }
    
    func downloadImageFrom(url: URL) {
            currentTask = URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }
                DispatchQueue.main.async {
                    let imageToCache = UIImage(data: data)
                    imageToCache.flatMap { [weak self] image in
                        self?.imageCache.setObject(image, forKey: url.absoluteString as NSString)
                        self?.image = image
                    }
                }
            }
            currentTask.resume()
    }
}
